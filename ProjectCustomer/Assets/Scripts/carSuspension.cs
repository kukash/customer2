﻿using UnityEngine;

public class carSuspension : MonoBehaviour
{

    public WheelCollider WheelL;
    public WheelCollider WheelR;
    public float AntiRoll = 10000f;
    public Rigidbody carRigidBody;

    [Range(0, 20)]
    public float naturalFrequency = 10;

    [Range(0, 3)]
    public float dampingRatio = 0.8f;

    [Range(-1, 1)]
    public float forceShift = 0.03f;
    public bool setSuspensionDistance = true;

    // Use this for initialization
    void Start()
    {
        carRigidBody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void fixedUpdate()
    {
        

        foreach (WheelCollider wc in GetComponentsInChildren<WheelCollider>())
        {
            JointSpring spring = wc.suspensionSpring;

            spring.spring = Mathf.Pow(Mathf.Sqrt(wc.sprungMass) * naturalFrequency, 2);
            spring.damper = 2 * dampingRatio * Mathf.Sqrt(spring.spring * wc.sprungMass);

            wc.suspensionSpring = spring;

            Vector3 wheelRelativeBody = transform.InverseTransformPoint(wc.transform.position);
            float distance = GetComponent<Rigidbody>().centerOfMass.y - wheelRelativeBody.y + wc.radius;

            wc.forceAppPointDistance = distance - forceShift;

            // the following line makes sure the spring force at maximum droop is exactly zero
            if (spring.targetPosition > 0 && setSuspensionDistance)
            {
                wc.suspensionDistance = wc.sprungMass * Physics.gravity.magnitude / (spring.targetPosition * spring.spring);
            }
        }

        /*
                WheelHit hit = new WheelHit();
                float travelL = 1.0f;
                float travelR = 1.0f;

                bool groundedL = WheelL.GetGroundHit(out hit);
                if (groundedL)
                {
                    travelL = (-WheelL.transform.InverseTransformPoint(hit.point).y - WheelL.radius) / WheelL.suspensionDistance;
                }

                bool groundedR = WheelR.GetGroundHit(out hit);
                if (groundedR)
                {
                    travelR = (-WheelR.transform.InverseTransformPoint(hit.point).y - WheelR.radius) / WheelR.suspensionDistance;
                }

                float antiRollForce = (travelL - travelR) * AntiRoll;

                if (groundedL)
                {
                    carRigidBody.AddForceAtPosition(WheelL.transform.up * -antiRollForce, WheelL.transform.position);
                }
                if (groundedR)
                {
                    carRigidBody.AddForceAtPosition(WheelR.transform.up * antiRollForce, WheelR.transform.position);
                }
                */
    }

}