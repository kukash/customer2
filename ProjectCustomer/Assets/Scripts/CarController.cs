﻿using UnityEngine;


[RequireComponent(typeof(InputManager))]
public class CarController : MonoBehaviour
{
    [SerializeField] private InputManager input;
    [SerializeField] private float maxTurn = 20f;
    [SerializeField] private Rigidbody rb;
    [Range(0, 5)]
    [SerializeField] private float speed = 1;
    [Range(0, 1)]
    [SerializeField] private float steeringAngle = 1f;
    [Range(0, 1)]
    [SerializeField] private float driftValue = 1f;
    private float acc = 0;
    [Range(0, 1)]
    [SerializeField] private float grip = 1f;
    private float angle;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        input = GetComponent<InputManager>();
    }


    private void move()
    {

        transform.Rotate(0, angle, 0);
        Mathf.Clamp(acc, 0, 1);
        rb.velocity += transform.forward * acc;
        while (rb.velocity.magnitude > 20)
        {
            rb.velocity *= 0.9f;
        }
    }
    private void getInput()
    {
        acc = input.accelerate;
        acc *= speed;
        angle = transform.forward.y + input.steer * steeringAngle * rb.velocity.magnitude/2;

    }
    private void friction()
    {
        acc *= grip;
    }
    void FixedUpdate()
    {
        getInput();
        friction();
        move();
        Debug.Log(rb.velocity.magnitude);

    }
}
