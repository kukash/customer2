﻿using UnityEngine;

public class InputManager : MonoBehaviour
{

    public float accelerate;
    public float steer;


    // Update is called once per frame
    void FixedUpdate()
    {
        accelerate = Input.GetAxisRaw("Vertical");
        steer = Input.GetAxisRaw("Horizontal");
    }
}
